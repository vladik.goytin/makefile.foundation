# Regular root arch.mk file to start with.

ARCH_FLAGS_x86_64-linux-gnu :=							\
	-march=native

ifneq ($(ROOTDIR),$(CURDIR))
-include $(CURDIR)/arch.mk
endif
include $(ROOTDIR)/makefile.foundation/mk/common.mk
