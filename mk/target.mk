# SPDX-FileCopyrightText: (C) 2020 Vladik Goytin <goytin@gmail.com>
#
# SPDX-License-Identifier: Apache-2.0

# Makefile Foundation.
# target.mk

# Need to get the architecture and compiler version either from $(CC) or
# from $(CXX). The logic assumes that both $(CC) and $(CXX) should return
# the same architecture and version. Anyway CXX has a precedence over CC
# according to the table below
# (!default contains all $(origin ) variants which are not default):
#
#	1. CC/default, CXX/default	--> take from CC
#	2. CC/!default, CXX/defaut	--> take from CC
#	3. CC
#
#				CC default	|	CC !default
#	===========================================================
#	CXX default	|	CC		|	CC
#	CXX !default	|	CXX		|	CXX
#
ifneq ($(filter-out $(origin CXX),default),)
compiler :=		$(CXX)
else
compiler :=		$(CC)
endif

# Assuming that the compiler knows how to execute the commands below.
arch :=			$(call safe_shell, $(compiler) -dumpmachine)
comp_ver :=		$(call safe_shell, $(compiler) -dumpversion)
TARGET_ID :=		$(arch)-$(comp_ver)$(TARGET_TYPE)

ifeq ($(filter-out $(origin ARCH_FLAGS_$(arch)),undefined),)
$(warning Architecture $(arch) is not supported)
$(warning Please set ARCH_FLAGS_$(arch) variable in arch.mk)
$(error )
endif

TARGET_ARCH :=		$(value ARCH_FLAGS_$(arch))
