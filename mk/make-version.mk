# SPDX-FileCopyrightText: (C) 2020 Vladik Goytin <goytin@gmail.com>
#
# SPDX-License-Identifier: Apache-2.0

# Makefile Foundation.
# make-version.mk

# This version .SHELLSTATUS feature.
oldest_make :=			4.2

ifneq ($(oldest_make), $(word 1, $(sort $(MAKE_VERSION) $(oldest_make))))
$(info Your make version $(MAKE_VERSION) is too old.)
$(info At least make version $(oldest_make) is required.)
$(info TODO additional help.)
$(error )
endif
