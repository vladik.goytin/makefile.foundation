# SPDX-FileCopyrightText: (C) 2020 Vladik Goytin <goytin@gmail.com>
#
# SPDX-License-Identifier: Apache-2.0

# Makefile Foundation.
# control-vars.mk

# $1 -- variable name
# $2 -- help message
# $3 -- default value
define set_mf_var
$1_msg = $(shell printf '%-20s' $1:=1)-- $(strip $2). Default: $(strip $3)
endef

$(eval $(call set_mf_var,MF_NO_CCACHE,						\
		Disable using ccache, enabled.))
$(eval $(call set_mf_var,MF_DEBUG,						\
		Enable debug compilation, disabled.))
$(eval $(call set_mf_var,MF_NO_LTO,						\
		Disable Link Time Optimization, enabled.))
$(eval $(call set_mf_var, MF_NO_SECTIONS,					\
		Disable -ffunction/data-sections, enabled.))
$(eval $(call set_mf_var, MF_LD_GOLD,						\
		Use ld.gold linker. Desirable for C++, disabled.))

# Support various weird compilers like ARMCC.
$(eval $(call set_mf_var, MF_NO_GCC,						\
		Ignore all GCC-specific flags; use only relevant ARCH_FLAGS_,	\
			disabled.))
