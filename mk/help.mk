# SPDX-FileCopyrightText: (C) 2020 Vladik Goytin <goytin@gmail.com>
#
# SPDX-License-Identifier: Apache-2.0

# Makefile Foundation.
# help.mk

target_help =									\
	$(info Important targets)						\
	$(info =================$(\n))						\
	$(info $(nil)  all       -- build $(TARGET) for selected architecture)	\
	$(info $(nil)  clean     -- clean selected architecture build)		\
	$(info $(nil)  distclean -- clean all builds)				\
	$(info $(nil)  help      -- show this help$(\n))

arch_help =									\
	$(info Supported architectures)						\
	$(info =======================$(\n))					\
	$(foreach a,								\
		$(subst ARCH_FLAGS_,,$(filter ARCH_FLAGS_%,$(.VARIABLES))),	\
		$(info $(nil)  $(a)))						\
	$(info $(\n)To add a new architecture xxx set variable ARCH_FLASG_xxx in arch.mk.) \
	$(info See <your-compiler> -dumpmachine command for more details.$(\n))

link_type_help =								\
	$(info Supported link types)						\
	$(info ====================)						\
	$(info To be used as "LINK_TYPE = <link_type>" in Makefile$(\n))	\
	$(foreach t,								\
		$(subst CODEGEN_,,$(filter CODEGEN_%,$(.VARIABLES))),		\
		$(info $(nil)  $(t)))

control_var_help =								\
	$(info $(\n)Control variables)						\
	$(info =================)						\
	$(info To be used as "make <MF_var>:=1 ..."$(\n))			\
	$(foreach v, $(filter MF_%_msg,$(.VARIABLES)), $(info $(nil)  $(value $v)))

.PHONY:	help
help:
	$(call target_help)
	$(call arch_help)
	$(call link_type_help)
	$(call control_var_help)
	@:
