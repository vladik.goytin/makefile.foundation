# SPDX-FileCopyrightText: (C) 2020 Vladik Goytin <goytin@gmail.com>
#
# SPDX-License-Identifier: Apache-2.0

# Makefile Foundation.
# debug-release.mk

ifeq ($(MF_DEBUG),1)

DBG_FLAGS$(MF_NO_GCC) :=	-g3 -gdwarf-4 -fvar-tracking-assignments
OPT_FLAGS$(MF_NO_GCC) :=	-Og
LD_OPT_FLAGS :=
LTO_FLAGS :=
RELEASE_WARN_FLAGS :=
TARGET_TYPE :=			.debug

else

DBG_FLAGS$(MF_NO_GCC) :=	-s -DNDEBUG

SECT_FLAGS$(MF_NO_GCC)$(MF_NO_SECTIONS) :=				\
				-ffunction-sections -fdata-sections
LD_SECT_FLAGS$(MF_NO_GCC)$(MF_NO_SECTIONS) :=				\
				-Wl,--gc-sections

LTO_FLAGS$(MF_NO_GCC)$(MF_NO_LTO) :=					\
				-flto -fuse-linker-plugin
OPT_FLAGS$(MF_NO_GCC) :=	-Os -fomit-frame-pointer $(SECT_FLAGS)	\
					$(LTO_FLAGS)
LD_OPT_FLAGS$(MF_NO_GCC) :=	-Wl,-O1 $(LD_SECT_FLAGS)
RELEASE_WARN_FLAGS$(MF_NO_GCC) :=					\
				-D_FORTIFY_SOURCE=2
TARGET_TYPE :=

endif
