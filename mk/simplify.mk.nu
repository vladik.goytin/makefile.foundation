# SPDX-FileCopyrightText: (C) 2020 Vladik Goytin <goytin@gmail.com>
#
# SPDX-License-Identifier: Apache-2.0

# Makefile Foundation.
# ccache.mk

# Do not trust the user: simplify variables.

# Mandatory variables.
override TARGET :=		$(TARGET)
override LINK_TYPE :=		$(LINK_TYPE)

# Intentionally do not simplify SRCS as I not always need it.

# Optional variables.
override INC_FLAGS :=		$(INC_FLAGS)
override MISC_FLAGS :=		$(MISC_FLAGS)
override LDFLAGS :=		$(LDFLAGS)
override LDLIBS :=		$(LDLIBS)
