# SPDX-FileCopyrightText: (C) 2020 Vladik Goytin <goytin@gmail.com>
#
# SPDX-License-Identifier: Apache-2.0

# Makefile Foundation.
# make-version.mk

all:				$(RESULT)

$(RESULT):			$(OBJS)
	$(LINK)

gen_dep_file$(MF_NO_GCC) =	-MD -MF $(call obj_to_d,$@)
COMPILE.c =			$(CCACHE) $(CC) $(CFLAGS) $(CPPFLAGS)		\
					$(TARGET_ARCH) -c $(gen_dep_file)
COMPILE.cc =			$(CCACHE) $(CXX) $(CXXFLAGS) $(CPPFLAGS)	\
					$(TARGET_ARCH) -c $(gen_dep_file)

$(OBJS):			$(MAKEFILE_LIST) | $(OBJ_DIRS)
$(OBJ_DIRS):
	mkdir -p $@

$(BUILD_DIR)/$(TARGET_ID)/%.o:	%.c $(MAKEFILE_LIST)
	$(COMPILE.c) $(OUTPUT_OPTION) $<
$(BUILD_DIR)/$(TARGET_ID)/%.o:	%.cc $(MAKEFILE_LIST)
	$(COMPILE.cc) $(OUTPUT_OPTION) $<
$(BUILD_DIR)/$(TARGET_ID)/%.o:	%.cpp $(MAKEFILE_LIST)
	$(COMPILE.cc) $(OUTPUT_OPTION) $<

LINK.c =			$(CC) $(LDFLAGS_S) $(TARGET_ARCH) $^		\
					$(LDLIBS) -o $@
LINK.c++ =			$(CXX) $(LDFLAGS_S) $(TARGET_ARCH) $^		\
					$(LDLIBS) -o $@
LINK.static_library =		$(AR) -rcv $@ $^
LINK.shared_library =		$(if $(CPP_FILES),$(LINK.c++),$(LINK.c))
LINK.SHARED_LIBRARY =		$(LINK.shared_library)

LINK =				$(LINK.$(LINK_TYPE))

.PHONY:	clean distclean
clean:
	$(RM) -r $(BUILD_DIR)/$(TARGET_ID)
distclean:
	$(RM) -r $(BUILD_DIR)
