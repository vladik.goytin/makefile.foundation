# SPDX-FileCopyrightText: (C) 2020 Vladik Goytin <goytin@gmail.com>
#
# SPDX-License-Identifier: Apache-2.0

# Makefile Foundation.
# ld.mk

ifeq ($(MF_LD_GOLD),1)
LD_GOLD$(MF_NO_GCC) :=		-fuse-ld=gold
endif
