# SPDX-FileCopyrightText: (C) 2020 Vladik Goytin <goytin@gmail.com>
#
# SPDX-License-Identifier: Apache-2.0

# Makefile Foundation.
# libmk.mk

# Nothing.
# Useful in constructs like "$(info $(nil)  text)" to add leading spaces.
nil :=

# New line.
define \n


endef

comma := ,

# Safe $(shell): reports a faulty command and exits.
#
#	$(shell <shell command>)
#
# is replaced to
#
#	$(call safe_shell, <shell command>)
#
# Note the comma after "safe_shell".
#
# Example:
#
#	header :=	$(shell jq '.header |				\
#					.title,				\
#					.project' $(CONF))
#
# which may silently fail becomes
#
#	header :=	$(call safe_shell, jq '.header |		\
#					.title$(comma)			\
#					.project' $(CONF))
#
# Note, that it is impossible to use character ',' directly in $(safe_shell)
# as it would change number of its parameters.

safe_shell =		$(shell $1)$(if $(filter-out 0,$(.SHELLSTATUS)),	\
						$(error Failed command: $1),)
