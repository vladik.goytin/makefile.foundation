# SPDX-FileCopyrightText: (C) 2020 Vladik Goytin <goytin@gmail.com>
#
# SPDX-License-Identifier: Apache-2.0

# Makefile Foundation.
# code-generation.mk

CODEGEN_c :=
CODEGEN_c++ :=
CODEGEN_static_library :=
CODEGEN_shared_library :=	-fpic
CODEGEN_SHARED_LIBRARY :=	-fPIC

LD_CODEGEN_c :=
LD_CODEGEN_c++ :=
LD_CODEGEN_static_library :=
LD_CODEGEN_shared_library :=	-shared -Wl,-soname,$(TARGET)
LD_CODEGEN_SHARED_LIBRARY :=	-shared -Wl,-soname,$(TARGET)

ifeq ($(origin CODEGEN_$(LINK_TYPE)),undefined)
code_gen_types := $(subst CODEGEN_,,$(filter CODEGEN_%,$(.VARIABLES)))
$(info Invalid LINK_TYPE: $(LINK_TYPE))
$(info Please set LINK_TYPE to one of: $(code_gen_types))
$(info See $(makefiles_dir)docs/link-type.txt for more details.)
$(error )
endif

CODE_GEN_FLAGS :=		$(value CODEGEN_$(LINK_TYPE))
LD_CODE_GEN_FLAGS :=		$(value LD_CODEGEN_$(LINK_TYPE))
