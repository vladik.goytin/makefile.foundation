# SPDX-FileCopyrightText: (C) 2020 Vladik Goytin <goytin@gmail.com>
#
# SPDX-License-Identifier: Apache-2.0

# Makefile Foundation.
# common.mk

makefiles_dir :=		$(dir $(lastword $(MAKEFILE_LIST)))
BUILD_DIR :=			build


include $(makefiles_dir)libmk.mk
include $(makefiles_dir)code-generation.mk

help_status :=		$(if $(or						\
						$(findstring undefined,		\
							$(flavor MAKECMDGOALS)),\
						$(findstring help,		\
							$(MAKECMDGOALS))),	\
					NEED_HELP_TARGET,)

ifeq ($(strip $(help_status)), NEED_HELP_TARGET)

include $(makefiles_dir)control-vars.mk
include $(makefiles_dir)help.mk

else

include $(makefiles_dir)make-version.mk
include $(makefiles_dir)ccache.mk
include $(makefiles_dir)ld.mk
include $(makefiles_dir)debug-release.mk
include $(makefiles_dir)warn.mk
include $(makefiles_dir)target.mk		# TARGET_ID, TARGET_ARCH

RESULT :=			$(BUILD_DIR)/$(TARGET_ID)/$(TARGET)

CFLAGS :=			$(INC_FLAGS) $(DBG_FLAGS) $(OPT_FLAGS)		\
					$(WARN_FLAGS) $(CODE_GEN_FLAGS)		\
					$(MISC_FLAGS)
CXXFLAGS :=			$(CFLAGS)
LDFLAGS_S :=			$(LDFLAGS) $(DBG_FLAGS) $(OPT_FLAGS)		\
					$(LD_OPT_FLAGS)	$(LD_CODE_GEN_FLAGS)	\
					$(LD_GOLD)	

obj_to_d =			$(dir $(1)).$(notdir $(1)).d

files_by_ext =			$(patsubst %.$(1),%.o,$(filter %.$(1),$(SRCS)))
CPP_FILES :=			$(call files_by_ext,cpp)
OBJS :=				$(addprefix $(BUILD_DIR)/$(TARGET_ID)/,		\
					$(call files_by_ext,c) $(CPP_FILES))
OBJ_DIRS :=			$(sort $(dir $(OBJS)))
DEPS :=				$(foreach obj,$(OBJS),$(call obj_to_d,$(obj)))

include $(makefiles_dir)rules.mk

-include $(DEPS)

endif
