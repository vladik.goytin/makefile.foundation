# SPDX-FileCopyrightText: (C) 2020 Vladik Goytin <goytin@gmail.com>
#
# SPDX-License-Identifier: Apache-2.0

# Makefile Foundation.
# warn.mk

WARN_FLAGS$(MF_NO_GCC) :=	-Wall -Wextra -Wshadow -Wwrite-strings -Wformat	\
					-Wformat-security -Wstrict-aliasing	\
					-Wshadow -Wpointer-arith		\
					-Wno-unused-parameter -Werror=format=2	\
					$(RELEASE_WARN_FLAGS)
