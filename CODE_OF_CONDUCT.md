Code of Conduct
===============

1. This is the Code of Conduct of the Makefile Foundation project.
2. Any change in the Code of Conduct is prohibited.
3. Anyone who proposes a change in the Code of Conduct is expelled from the Makefile Foundation project.
